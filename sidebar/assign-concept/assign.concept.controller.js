const bluebird = require('bluebird')
const mapClient = require('map-client')
//const handleNode = require('map-tree/extensions/node/handle.node')
const handleTree = require('../../extensions/map/handle.tree')

module.exports = function (ngModule, map) {
    console.log('attaching assignConceptController')
    ngModule.controller("assignConceptController", ['$scope', 'mapService', '$routeParams',
        function ($scope, mapService, $routeParams) {
            //Fetch data based on the route parameters
            const legendId = $routeParams.legendId
            console.log('entered assign concept controller')
            $scope.M = mapService
            $scope.getFocus = false

            var concepts = map._editor._tree.graphJson.legend.nodes

            mapService.fetchOnScope($scope)

            $scope.autoCompleteConceptOptions = {
                minimumChars: 0,
                activateOnFocus: true,
                noMatchTemplateEnabled: false,
                data: function (searchText) {
                    searchText = searchText.toLowerCase();

                    var filtered = concepts.filter(function (concept) {
                        return concept.label.toLowerCase().search(searchText) >= 0
                    }).map(function (concept) {
                        return concept.label
                    })

                    return filtered
                }
            }

            $scope.assignConcept = function (node, conceptLabel) {
                /*
                    1. Check if there is a matching concept
                    2. If not present, then create the concept
                    3. Assign concept to the node
                */
                var assigned_concept = null
                findOrCreateConcept(conceptLabel)
                    .then(function (concept) {
                        assigned_concept = concept
                        return mapClient.assignNodeType(node.id, concept.id)
                    }).then(function (updated_node) {
                        //call handle on edit machina to take updated node and schema
                        //handleNode(map._editor._json, 'assigned_concept', updated_node, assigned_concept)
                        handleTree(map, 'assigned_concept', updated_node, assigned_concept)
                    })

                function findOrCreateConcept(label) {
                    var matching_concepts = concepts.filter(function (concept) {
                        return concept.label.toLowerCase() == label.toLowerCase()
                    })

                    if (matching_concepts.length == 1) {
                        return bluebird.resolve(matching_concepts[0])
                    } else {
                        return mapClient.createNewNodeType(label, legendId)
                            .then(function (new_concept) {
                                //handleNode(map._editor._json, 'created_concept', new_concept)
                                handleTree(map, 'created_concept', new_concept)
                                return bluebird.resolve(new_concept)
                            })
                    }
                }
            }

            $scope.containerStyle = {
                width: 200,
                position: 'relative',
                left: map.flatNodes[$routeParams.tid]._x,
                top: map.flatNodes[$routeParams.tid]._y + 30,
                minHeight: 30,
                zIndex: 3
            }

        }
    ])


}