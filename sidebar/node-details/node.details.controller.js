const handleNode = require('map-tree/extensions/node/handle.node')

module.exports = function (ngModule, editorScope) {

    ngModule.controller("nodeDetailsController", ['$scope', 'mapService',
        function ($scope, mapService) {
            $scope.M = mapService
            $scope.editing = false
            $scope.state = {
                editing: false,
                changed: false
            }

            mapService.fetchOnScope($scope)

            $scope.copyValues = function () {
                $scope.valuesCopy = angular.copy($scope.fetched.node.properties)
            }

            $scope.close = function () {
                handleNode(editorScope._json, 'close')
            }
        }
    ])

}