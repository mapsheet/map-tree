const autoComplete = require('angular-auto-complete')
module.exports = function (container, map) {
    map._editor._tree = map

    //add element into container for sidebar
    var sidebar = document.createElement('div')
    //sidebar.innerHTML = "<node-editor></node-editor>"
    sidebar.innerHTML = require('./sidebar.template.html') //"Sidebar <ng-view></ng-view>"
    container.appendChild(sidebar)

    var editor = angular.module('ngEditor', ['ngRoute', 'jsonFormatter', 'autoCompleteModule'])

    //routes

    require('./assign-concept/assign.concept.controller')(editor, map)
    require('./assign-relation/assign.relation.controller')(editor, map)

    require('./sidebar.routes')(editor)
    //controllers
    require('./sidebar.controller')(editor, map._editor)
    require('./node-details/node.details.controller')(editor, map._editor)
    //services
    require('map-tree/sidebar/map.service')(editor, map)

    //require('./controllers/concept.assign.controller')(editor, editorScope)
    //require('map-tree/sidebar/node.editor.directive')(editor, editorScope)

    setTimeout(function () {

        angular.bootstrap(sidebar, ['ngEditor']);
    }, 100)
}