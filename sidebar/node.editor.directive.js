const mapClient = require('map-client')
const handleNode = require('map-tree/extensions/handle.node')

module.exports = function (ngModule, editorScope) {

    ngModule.directive('nodeEditor', function () {
        return {
            template: require('./node.editor.template.html'),
            controller: ['$scope', '$timeout', function ($scope, $timeout) {
                editorScope.refresh = function () {
                    //calling timeout redraws the angular portion
                    $timeout(function () {
                        {}
                    }, 0)
                }
                $scope.editor = editorScope

                $scope.copyValues = function () {
                    $scope.editor.valuesCopy = angular.copy($scope.editor.values)
                }
                console.log('editorScope is ', editorScope)

                $scope.saveProperties = function () {
                    var originalValues = $scope.editor.values
                    var updatedValues = $scope.editor.valuesCopy
                    var id = $scope.editor.current.node.id

                    mapClient.updateNodeProperties(id, updatedValues)
                        .then(function () {
                            console.log('updated ')
                            Object.assign(originalValues, updatedValues)
                            $scope.editing(false)
                        })
                }

                $scope.editing = function (is_editing) {
                    console.log('setting editing as ', is_editing)
                    $scope.editor.state.editing = is_editing
                    if (is_editing) {
                        handleNode($scope.editor.current, 'editing')
                    } else {
                        handleNode($scope.editor.current, 'viewing')
                    }

                    editorScope.refresh()

                }
            }]
        }
    })
}