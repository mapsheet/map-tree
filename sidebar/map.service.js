const to_fetch = {
    nodeId: {
        as: 'node',
        from: 'nodes'
    },
    edgeId: {
        as: 'edge',
        from: 'edges'
    },
    conceptId: {
        as: 'concept',
        from: 'nodeTypes'
    },
    relationId: {
        as: 'relation',
        from: 'edgeTypes'
    }
}

module.exports = function (ngModule, map) {

    ngModule.service('mapService', ['$routeParams', '$http', function ($routeParams, $http) {
        this.fetchOnScope = function (scope) {
            var json = {}
            scope.fetched = json
            Object.keys(to_fetch).map(function (param) {
                if ($routeParams[param]) {
                    if ($routeParams[param] != 'root') {
                        $http.get('/api/' + to_fetch[param].from + '/' + $routeParams[param])
                            .then(function (response) {
                                json[to_fetch[param].as] = response.data
                                console.log('got data for ', param, response.data)
                            })
                    }

                }
            })
        }

        this.lockMap = function () {
            console.log('locking map')
            map.lock()
        }

        this.unlockMap = function () {
            map.unlock()
        }
    }])
}