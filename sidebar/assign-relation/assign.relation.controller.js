const mapClient = require('map-client')
const handleTree = require('map-tree/extensions/map/handle.tree')

module.exports = function (ngModule, map) {
    console.log('attaching assignRelationController')
    ngModule.controller("assignRelationController", ['$scope', 'mapService', '$routeParams',
        function ($scope, mapService, $routeParams) {
            $scope.M = mapService
            mapService.fetchOnScope($scope)
            var tnode = map.flatNodes[$routeParams.tid]
            $scope.tnode = tnode

            $scope.containerStyle = {
                width: 200,
                position: 'relative',
                left: tnode._x - 200,
                top: tnode._y + 10,
                minHeight: 30,
                zIndex: 3
            }

            $scope.assignRelation = function (from_node, to_node, edge, relation_label) {
                console.log('edge ', edge, relation_label)
                var attached_relation = null
                mapClient.createRelation(from_node, to_node, $routeParams.legendId, relation_label)
                    .then(function (relation) {
                        attached_relation = relation
                        handleTree(map, 'created_relation', relation)
                        return mapClient.assignRelation(edge, relation)
                    }).then(function (updated_edge) {
                        handleTree(map, 'assigned_relation', edge, attached_relation)
                    })
            }
        }
    ])
}