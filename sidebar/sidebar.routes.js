module.exports = function (ngModule, editorScope) {

    ngModule.config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when("/", {
                template: "MAIN",
                controller: 'sidebarController'
            }).when("/:legendId/:graphId/:tid/concept/assign/:nodeId", {
                template: require('./assign-concept/assign.concept.html'),
                controller: 'assignConceptController'
            }).when("/:legendId/:graphId/:tid/relation/assign/:nodeId/:edgeId", {
                template: require('./assign-relation/assign.relation.html'),
                controller: 'assignRelationController'
            }).when("/:legendId/:graphId/:tid/tnode/:conceptId/:relationId/:nodeId/:edgeId", {
                template: require('./node-details/node.details.html'),
                controller: 'nodeDetailsController'
            })
        /*
        .when("/:legendId/:graphId/concept/edit/:conceptId", {
            template: require('./edit-concept/edit.concept.html'),
            controller: 'sidebarController'
        }).when("/:legendId/:graphId/relation/edit/:conceptId", {
            template: require('./edit-relation/edit.relation.html'),
            controller: 'sidebarController'
        })
        */
    }]);



}