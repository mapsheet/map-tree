const machina = require('machina')
const $ = require('jquery')
const handleNode = require('../node/handle.node')

const computeAttachConnnectables = require('../../compute/compute.attach.connectables')

module.exports = function (map) {
    return new machina.BehavioralFsm({
        initialize: function () {
            console.log('extend machina initialized')
        },
        initialState: 'uninitialized',
        states: {
            uninitialized: {
                initialize: 'idle'
            },
            idle: {
                _onEnter: function () {
                    console.log('extend machina is now in idle')
                },
                extend: 'extending',
                keypress: function (client, key, e) {

                    // console.log('handling keypress from tree in idle state ', map._focus)
                    if (map._focus) {
                        //while extending handle event only for the node which is being edited
                        handleNode(map.flatNodes[map._focus], key + '_key', e)
                        //e.preventDefault()
                    }
                }
            },
            extending: {
                _onEnter: function (client) {

                    var focus_node = map.flatNodes[map._focus]
                    console.log('extend machina Entering extend state')
                    computeAttachConnnectables(map)
                    //console.log('connectables for this node are ', map.connectables)
                    //nodeMachina.expand(focus_node)
                    handleNode(focus_node, 'expand')
                    map.draw()
                    if (map.connectables && map.connectables.length > 0) {
                        map.inputFocus(map.connectables[0])
                    }
                    //map.transition('extending')
                },
                view: 'viewing',
                keypress: function (client, key, e) {

                    // console.log('handling keypress from tree in extending state ', map._inputFocus)
                    if (map._inputFocus) {
                        //while extending handle event only for the node which is being edited
                        console.log(map.flatNodes[map._inputFocus])
                        handleNode(map.flatNodes[map._inputFocus], key + '_key', e)
                        //e.preventDefault()
                    }
                },
                change_focus: 'idle',
                _onExit: function () {
                    map._manipulator.detachConnectables()
                    map.draw()
                }
            }
        },
        create: function (client) {
            this.handle(client, 'initialize')
        }
    })
}