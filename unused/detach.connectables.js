const _ = require('lodash')
const iterateTree = require('map-tree/utils/iterate.tree')

module.exports = function (map) {

    return function () {
        map._inputFocus = null

        iterateTree(map.treeJson, function (json) {
            json.children.filter(function (child) {
                return child._connectable
            }).map(function (node) {
                node._renderer.remove()
            })

            _.remove(json.children, function (child) {
                return child._connectable
            })
        })
    }
}