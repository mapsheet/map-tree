const machina = require('machina')
const nodeMachina = require('map-tree/node.machina')
const filterCloneTree = require('map-tree/utils/filter.clone.tree')
const iterateTree = require('./utils/iterate.tree')
const computeLayout = require('map-tree/layout/compute.layout')
const assignLayout = require('map-tree/layout/assign.layout')
const redrawConnectors = require('map-tree/render/redraw.connectors')
//const attachConnectables = require('./layout/attach.connectables')
// const detachConnectables = require('map-tree/manipulate/detach.connectables')
const attachSidebar = require('./sidebar/attach.sidebar')
const handleNode = require('map-tree/extensions/node/handle.node')
const computeSiblings = require('./compute/compute.siblings')
const handleKeypress = require('./render/handle.key.press')
const _ = require('underscore')
const manipulator = require('./manipulate')
const attachMapExtensions = require('./extensions/map/attach.map.extensions')
const handleTree = require('./extensions/map/handle.tree')
const attachLegend = require('./legend')

var throttledPan = null
const computeAttachConnectables = require('./compute/compute.attach.connectables')

const width = 100,
    height = 30,
    hgap = 150,
    vgap = 5



module.exports = machina.Fsm.extend({
    initialize: function (tree_json, graph_json, container, is_edit) {
        // //console.log('container in map tree ', container)
        ////console.log('initialize calld with ', tree_json, graph_json)
        const map = this
        map._id = graph_json.id
        map.treeJson = tree_json
        map.graphJson = graph_json
        map.flatNodes = {}
        map.connectables = []
        map._container = container
        map._nodeMachina = nodeMachina
        map._editor = {}
        map._editing = false
        map._allow_edit = is_edit
        // map._sideBar = attachSidebar(container, map)

        //attach connectables for all nodes


        map._manipulator = manipulator(map)
        attachMapExtensions(map)
        attachLegend(map, container)
        //map._simple = simpleMachine(map)

        handleKeypress(map)

        console.log('tree Json', map.treeJson)
        if (!throttledPan) {
            throttledPan = _.throttle(panToFocus(map), 500, {
                'trailing': false,
                'leading': true
            })
        }


        // iterateTree(tree_json, computeSiblings)
        if (tree_json.tid) {
            //tree is not empty
            map.handle('nonempty')
        } else {
            //tree is empty
            map.handle('empty')
        }

    },
    initialState: 'uninitialized',
    states: {
        uninitialized: {

            empty: 'empty',
            nonempty: 'initializing'
        },
        empty: {
            _onEnter: function () {
                //create a connectable for the root node
                //TODO - handle case where root node has a schema
                const map = this
                console.log('entering empty state')
                Object.assign(map.treeJson, {
                    tid: 'root',
                    name: 'Start with',
                    _connectable: true,
                    _shown: true,
                    children: [],
                    _isRoot: true
                })
                createMachinas(map)(map.treeJson)
                map.draw()
                map.focus(map.treeJson.tid)
                panToFocus(map)()
                map.transition('viewing')
            }
        },
        initializing: {
            _onEnter: function () {
                const map = this

                attachConnectables(map)
                //focus on the root node
                map.focus(map.treeJson.tid)

                console.log('entering non empty state')
                iterateTree(map.treeJson, createMachinas(map))
                iterateTree(map.treeJson, expandOrCollapse)


                setTimeout(function () {
                    panToFocus(map)()
                }, 1000)

                map.transition('viewing')
            }
        },
        viewing: {
            extend: function () {
                // console.log('handing extend on tree')
                const map = this
                // if (map._focus) {
                //     map.transition('extending')
                // }
                handleTree(map, 'extend')
            },
            lock: 'locked',
            keypress: function (key, e) {
                const map = this
                // console.log('handling keypress from tree in viewing state')
                // if (map._focus) {
                //     //handleNode(map.flatNodes[map._focus], key + '_key', e)
                //     //e.preventDefault()
                // }
                //handleTree(map, key + '_key', e)
                //handleTree(map, 'keypress', key, e)

            },
            focus: function (tid) {
                const map = this
                map.focus(tid)
            }
        },
        locked: {
            _onEnter: function () {
                const map = this
                map._container.style.opacity = 0.9
            },
            unlock: 'viewing',
            _onExit: function () {
                const map = this
                map._container.style.opacity = 1
            }
        }
    },
    draw: function () {
        /*
            1. Create a pruned json with only the nodes to be shown
            2. compute layout for these nodes
            3. Assign the computed coordinates to nodes on the original json

        */
        const map = this
        toggleConnectables(map)

        var pruned_json = filterCloneTree(map.treeJson, function (json) {
            return json._shown
        })

        computeSiblings(pruned_json, map.flatNodes)

        console.log('pruned json ', pruned_json)

        var layout = computeLayout(pruned_json, 'RightLogical', width, height, hgap, vgap)

        assignLayout(layout)
        redrawConnectors(map, pruned_json)
        if (throttledPan) {
            throttledPan()
        }
    },
    highlight: function (tid) {
        /*
            1. get json corresponding to tid
            2. get rootPath
            3. expand all nodes along the root path
        */
        const map = this
        var json = map.flatNodes[tid]
        var rootPath = json.rootPath
        //console.log('rootPath ', rootPath)
        rootPath.map(function (path_tid) {
            nodeMachina.handle(map.flatNodes[path_tid], 'expand')
        })
    },
    focus: function (tid) {
        //console.log('in map.focus for ', tid)
        const map = this
        if (map._focus == tid) {
            //this tid is already in focus
            return
        }
        if (map._focus) {
            //nodeMachina.loseFocus(map.flatNodes[map._focus])
            handleNode(map.flatNodes[map._focus], 'lose_focus')
        }

        map._focus = tid

        setTimeout(function () {


            var focused_node = map.flatNodes[map._focus]
            handleNode(map.flatNodes[map._focus], 'gain_focus')
            handleTree(map, 'change_focus')
            throttledPan()

            map.handle('view')
            map.draw()
        })
    },
    extend: function () {
        const map = this
        console.log('handling extend on tree')
        map.handle('extend')
    },
    handleKeypress: function (key, e) {
        this.handle('keypress', key, e)
    },
    lock: function () {
        this.handle('lock')
    },
    unlock: function () {
        this.handle('unlock')
    },
    assign: function (tid) {
        handleTree(this, 'assign', tid)
    }
})

function panToFocus(map_tree) {

    return function () {
        //console.log('calling pan to node')
        setTimeout(function () {
            if (!map_tree._focus) {
                return
            }

            var x = map_tree.flatNodes[map_tree._focus]._x,
                y = map_tree.flatNodes[map_tree._focus]._y
            var centerx = window.innerWidth / 4,
                centery = window.innerHeight / 2
            var movex = centerx - x,
                movey = centery - y
            //console.log('x ', x, 'y ', y, ' centerx ', centerx, ' centery ', centery, ' movex ', movex, ' movey ', movey)
            var transformation = 'translate(' + movex + 'px,' + movey + 'px)'
            //console.log('transformation ', transformation)
            var container = document.getElementById('container')
            //container.style.transform = transformation
            // $('#container').animate({
            //     transform: transformation
            // }, 1000, function () {
            //     //console.log('animation complete')
            // })

            //console.log('animating q transform to ', movex, movey)
            $("#container").animate({
                top: movey + 'px',
                left: movex + 'px'
            }, 500)
        }, 0)
    }

}

function toggleConnectables(map_tree) {
    iterateTree(map_tree.treeJson, function (json) {
        if (json._connectable) {
            if (map_tree._editing) {
                if (map_tree._focus == json.parent.tid && (!json.parent._expandable || json.parent._expanded)) {
                    //json._shown = true
                    handleNode(json, 'show')
                } else {
                    handleNode(json, 'hide')
                }
            } else {
                //hide connectables if not editing
                handleNode(json, 'hide')
            }

        }
    })
}

function attachConnectables(map_tree) {
    iterateTree(map_tree.treeJson, function (json) {
        computeAttachConnectables(map_tree, json)
    })
}

function createMachinas(map_tree) {
    return function (json) {
        map_tree.flatNodes[json.tid] = json
        nodeMachina.create(json, map_tree)
    }
}

function expandOrCollapse(json) {
    if (json.source) {
        handleNode(json, 'collapse')
    } else {
        handleNode(json, 'expand')
    }
}