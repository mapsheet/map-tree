const machina = require('machina')
const renderNode = require('./render/render.node')
var extension_fns = {
    branching: require('./extensions/node/branching.machina'),
    focus: require('./extensions/node/focus.machina'),
    populate: require('./extensions/node/populate.machina'),
    delete: require('./extensions/node/delete.machina')
    //edit: require('./extensions/node/edit.machina')
}


module.exports = new machina.BehavioralFsm({
    initialize: function () {

    },
    initialState: 'uninitialized',
    states: {
        uninitialized: {
            initialize: 'initialized'
        },
        initialized: {
            show: 'showing',
            hiding: 'hiding'
        },
        hiding: {
            _onEnter: function (client) {
                //mark node as hidden, so that it is not computed in the layout
                client._shown = false
                client._renderer.hide()
            },
            show: "showing"
        },
        showing: {
            _onEnter: function (client) {
                //show the 
                client._shown = true
                client._renderer.show()
            },
            hide: 'hiding'
        }

    },
    create: function (client, tree_map) {
        var nodeMachina = this

        var extensions = {}
        Object.keys(extension_fns).map(function (ext) {
            extensions[ext] = extension_fns[ext](client)
        })

        //create a dom entry for this node
        client._tree = tree_map


        if (!client._renderer) {
            //dont create renderer if it already exists
            //this means existing node will be overwritten instead of creating a new one
            client._renderer = renderNode(client)
        }

        client._renderer.render()

        //Add all the machinas and client as extensions
        client.extensions = {
            node: {
                machina: nodeMachina,
                client: client
            }
        }
        Object.keys(extensions).map(function (ext) {
            var extension_client = {
                _node: client
            }
            extensions[ext].create(extension_client, client)
            client.extensions[ext] = {
                machina: extensions[ext],
                client: extension_client
            }
        })



        nodeMachina.handle(client, 'initialize')
    },
    draw: function (client, x, y) {
        client._renderer.drawAt(x, y)
    }
})