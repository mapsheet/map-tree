const mapClient = require('map-client')

module.exports = function (ngModule) {

    ngModule.directive('concept', function () {
        return {
            template: require('./concept.template.html'),
            scope: {
                conceptJson: '='
            },
            controller: ['$scope', '$timeout', function ($scope, $timeout) {
                $scope.state = {}
                $scope.newDescriptor = {}

                $scope.descriptorAdded = function (added_descriptor) {
                    console.log('added descriptor ', added_descriptor)
                    $scope.conceptJson.descriptors.push(added_descriptor)
                    $scope.state.adding = false

                    $timeout(function () {

                    }, 0)
                }

                var concept_json_on_server = null
                $scope.updateConcept = function () {
                    mapClient.updateConcept($scope.conceptJson)
                        .then(function (updated_concept) {
                            console.log('updated relation is ', updated_concept)
                            concept_json_on_server = angular.copy($scope.conceptJson)

                            //console.log('relationJson ', $scope.relationJson, angular.equals($scope.relationJson, rel_json_on_server))
                            $timeout(function () {}, 0)
                        })
                }

                $scope.changed = function () {
                    return !angular.equals($scope.conceptJson, concept_json_on_server)
                }

                $scope.readOnly = function () {
                    return getQueryStringValue('edit') != 'true'
                }

                $scope.colors = [{
                    name: 'Maroon',
                    code: 'maroon'
                }, {
                    name: 'Green',
                    code: 'green'
                }, {
                    name: 'Olive',
                    code: 'olive'
                }, {
                    name: 'Navy',
                    code: 'navy'
                }, {
                    name: 'Purple',
                    code: 'purple'
                }, {
                    name: 'Teal',
                    code: 'teal'
                }, {
                    name: 'Red',
                    code: 'red'
                }, {
                    name: 'Lime',
                    code: 'lime'
                }, {
                    name: 'Yellow',
                    code: 'yellow'
                }, {
                    name: 'Blue',
                    code: 'blue'
                }, {
                    name: 'Fuchsia',
                    code: 'fuchsia'
                }, {
                    name: 'Aqua',
                    code: 'aqua'
                }]

            }]


        }
    })
}

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}