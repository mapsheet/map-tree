module.exports = function (ngModule, map) {

    ngModule.controller("legendController", ['$scope',
        function ($scope) {
            $scope.M = map
            $scope.oneAtATime = true

            $scope.relationTitle = function (relation) {
                //return as fromLabel --relationLabel--> toLabel
                var fromConcept = map.graphJson.legend.nodes.find(function (concept) {
                    return concept.id == relation.fromId
                })

                var toConcept = map.graphJson.legend.nodes.find(function (concept) {
                    return concept.id == relation.toId
                })

                return fromConcept.label + ' -- ' + relation.label + ' --> ' + toConcept.label
            }
            //iterate and set relation title
            $scope.editChanged = function () {
                console.log($scope.M._editing)
                setTimeout(function () {
                    $scope.M.draw()
                }, 0)

            }

            $scope.toEdit = function () {
                return map._allow_edit
            }

            $scope.onSelect = function (item, model, label, event) {
                console.log('item, model, label, event ', item, model, label, event)
                window.location = window.location.pathname + '?id=' + getQueryStringValue('id') + '&perspective=' + item.id + '&edit=' + getQueryStringValue('edit')
            }
            $scope.graphJson = map.graphJson

            $scope.conceptStyle = function (concept_json) {
                return {
                    border: '2px solid ' + concept_json.color,
                    fontSize: '12px !important'
                }
            }

            $scope.homeLink = function () {
                return '?id=' + getQueryStringValue('id') + '&edit=' + getQueryStringValue('edit')
            }

            $scope.nodeCount = function (concept_json) {
                return map.graphJson.nodes.filter(function (node) {
                    return node.nodeTypeId == concept_json.id
                }).filter(function (node) {
                    return map.graphJson.edges.find(function (edge) {
                        //only return nodes which have some connectivity
                        return edge.fromId == node.id || edge.toId == node.id
                    })
                }).length
            }

            $scope.relevantNodes = function () {
                return map.graphJson.nodes.filter(function (node) {
                    return map.graphJson.edges.find(function (edge) {
                        //only return nodes which have some connectivity
                        return edge.fromId == node.id || edge.toId == node.id
                    })
                })
            }

            $scope.readOnly = function () {
                return getQueryStringValue('edit') != 'true'
            }
        }
    ])
}

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}