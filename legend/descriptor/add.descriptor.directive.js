const mapClient = require('map-client')
module.exports = function (ngModule) {

    ngModule.directive('addDescriptor', function () {
        return {
            template: require('./add.descriptor.template.html'),
            scope: {
                conceptId: '@',
                relationId: '@',
                afterAdding: '&'
            },
            controller: ['$scope', '$timeout', function ($scope, $timeout) {
                $scope.newDescriptor = {}
                $scope.addDescriptor = function (descriptor) {
                    var fn = null,
                        onId = null

                    if ($scope.conceptId) {
                        fn = mapClient.addConceptDescriptor
                        onId = $scope.conceptId
                    } else {
                        fn = mapClient.addRelationDescriptor
                        onId = $scope.relationId
                    }

                    fn(descriptor, onId)
                        .then(function (added_descriptor) {
                            if ($scope.afterAdding) {
                                $scope.afterAdding({
                                    added: added_descriptor
                                })
                            }
                        })

                }
            }]
        }
    })
}