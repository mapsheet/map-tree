module.exports = function (ngModule) {

    ngModule.directive('descriptor', function () {
        return {
            template: require('./descriptor.template.html'),
            scope: {
                descriptorJson: '='
            },
            controller: ['$scope', '$timeout', function ($scope) {

            }]
        }
    })
}