module.exports = function (map, container) {

    //add element into container for sidebar
    var legendElement = document.createElement('div')
    legendElement.innerHTML = require('./legend.template.html')
    container.appendChild(legendElement)

    var legendApp = angular.module('legendApp', ['jsonFormatter', "ui.bootstrap"])
    require('./legend.controller')(legendApp, map)
    require('./concepts/concept.directive')(legendApp)
    require('./relations/relation.directive')(legendApp)
    require('./descriptor/add.descriptor.directive')(legendApp)
    require('./descriptor/descriptor.directive')(legendApp)

    setTimeout(function () {

        angular.bootstrap(legendElement, ['legendApp']);
    }, 100)
}