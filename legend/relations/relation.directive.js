const mapClient = require('map-client')

module.exports = function (ngModule) {

    ngModule.directive('relation', function () {
        return {
            template: require('./relation.template.html'),
            scope: {
                relationJson: '='
            },
            controller: ['$scope', '$timeout', function ($scope, $timeout) {
                $scope.state = {}
                $scope.newDescriptor = {}

                $scope.descriptorAdded = function (added_descriptor) {
                    console.log('added descriptor ', added_descriptor)
                    $scope.relationJson.descriptors.push(added_descriptor)
                    $scope.state.adding = false

                    $timeout(function () {

                    }, 0)
                }

                $scope.cardinalities = [{
                    name: 'One to Many',
                    cardinality: 'one_to_many'
                }, {
                    name: 'One to One',
                    cardinality: 'one_to_one'
                }, {
                    name: 'Many to One',
                    cardinality: 'many_to_one'
                }, {
                    name: 'Many to Many',
                    cardinality: 'many_to_many'
                }]

                var rel_json_on_server = null
                $scope.saveServerJson = function () {
                    rel_json_on_server = angular.copy($scope.relationJson)
                }

                $scope.changed = function () {
                    return !angular.equals($scope.relationJson, rel_json_on_server)
                }

                $scope.updateRelation = function () {
                    mapClient.updateRelation($scope.relationJson)
                        .then(function (updated_relation) {
                            console.log('updated relation is ', updated_relation)
                            rel_json_on_server = angular.copy($scope.relationJson)

                            //console.log('relationJson ', $scope.relationJson, angular.equals($scope.relationJson, rel_json_on_server))
                            $timeout(function () {}, 0)
                        })
                }

                $scope.readOnly = function () {
                    return getQueryStringValue('edit') != 'true'
                }


            }]
        }
    })
}

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}