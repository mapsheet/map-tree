const MindmapLayouts = require('mindmap-layouts')

module.exports = function (root, layoutType, width, height, hgap, vgap) {
    //RightLogical

    var layout = new MindmapLayouts[layoutType](root, {
        getHeight: function (d) {
            // console.log('input to getHeight', d)
            return height
        },
        getWidth: function (d) {
            //console.log('input to getWidth', d)
            return width
        },
        getHGap: function () {
            return hgap
        },
        getVGap: function () {
            return vgap
        }

    }) // root is tree node like above

    return layout.doLayout()
}