const nodeMachina = require('map-tree/node.machina')

module.exports = function (layout) {
    /*
        1. Iterate through treeJson and hide all nodes
        2. Iterate through layout and draw nodes at that position
    */
    //iterateAndCall(treeJson, hide)

    iterateAndCall(layout, draw)

    function iterateAndCall(json, fn) {
        fn(json)
        json.children.map(function (child) {
            iterateAndCall(child, fn)
        })
    }


    function draw(json) {
        nodeMachina.draw(json.data, json.x, json.y)

    }
}