const iterateTree = require('../utils/iterate.tree')

module.exports = function (map) {

    return function (appendable) {
        console.log('inside attach appendable')
        if (!appendable) {

            return
        }


        iterateTree(map.treeJson, function (tnode) {
            //check where the tree node maps to appendable.at

            if (tnode._connectable || tnode.node.id != appendable.at) {
                return
            }

            var attach_tnode = {
                node: appendable.node,
                tid: Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5),
                source: appendable.edge,
                parent: tnode,
                _shown: true,
                _connectable: false,
                children: [],
                _incoming: (appendable.as == 'target')
            }

            if (appendable.node.label) {
                attach_tnode.name = appendable.node.label
            } else {
                if (appendable.node.schema) {
                    attach_tnode.name = appendable.node.properties[appendable.node.schema.titleKey]
                }
            }
            if (appendable.edge.schema) {
                attach_tnode.via = appendable.edge.schema.label
            }

            console.log('newly attached node ', attach_tnode)
            map._nodeMachina.create(attach_tnode, map)
            map.flatNodes[attach_tnode.tid] = attach_tnode

            //push this after the normal tree nodes
            var index_to_push = tnode.children.filter(function (child) {
                return !child._connectable
            }).length
            //add appendable node to this node
            tnode.children.splice(index_to_push, 0, attach_tnode)

        })
    }

}