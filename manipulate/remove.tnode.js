const handleNode = require('map-tree/extensions/node/handle.node')

module.exports = function (tree) {
    return function (tnode) {
        /*  
            1. remove using renderer
            2. remove tnode from children
            3. remove the corresponding edge from graph json
        */
        var parent = tnode.parent
        var source = tnode.source
        tnode._renderer.remove()

        if (parent) {
            removeFromArray(parent.children, function (child) {
                return child.tid == tnode.tid
            })
        }

        if (source) {
            removeFromArray(tree.graphJson.edges, function (edge) {
                return edge.id == source.tid
            })
        }

        tree.draw()
    }

    function removeFromArray(arr, matching_fn) {
        var index = arr.findIndex(matching_fn)
        if (index >= 0) {
            arr.splice(index, 1)
        }
    }
}