module.exports = function (tree) {
    return function (edge, relation, tnode) {
        /*
            1. Assign schema to the corresponding edge in graph json
            2. Assign source and via to the tnode
        */
        tnode.source.schema = relation
        tnode.source.edgeTypeId = relation.id
        tnode.via = relation.label

        tnode._tree._manipulator.refreshNode(tnode)
    }
}