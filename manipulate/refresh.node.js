const handleNode = require('map-tree/extensions/node/handle.node')

module.exports = function (tree) {
    return function (json, updated_tnode) {
        const nodeMachina = json._tree._nodeMachina
        Object.assign(json, updated_tnode)
        json._renderer.remove()
        delete json.extensions
        delete json.__machina__

        nodeMachina.create(json, json._tree)
        json._tree.flatNodes[json.tid] = json

        handleNode(json, 'show')
        json._tree.draw()
    }
}