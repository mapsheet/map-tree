const mapClient = require('map-client')

module.exports = function (tree) {
    return function (json, label) {

        var node = {
            label: label,
            graphId: tree._id,
            free: true,
            root: true
        }

        mapClient.createNode(node)
            .then(function (created_node) {
                var root_tnode = {
                    node: created_node,
                    tid: 'new_root',
                    name: created_node.label,
                    _shown: true,
                    _connectable: false,
                    children: []
                }
                tree._manipulator.refreshNode(json, root_tnode)


                tree.focus(json.tid)
            })
    }
}