const iterateTree = require('../utils/iterate.tree')
// const refreshNode = require('../manipulate/refresh.node')

module.exports = function (tree) {
    return function (json, updated_node, assigned_concept) {
        /*
            1. Find all tid where node.id is matching updated_node
            2. refresh node with new schema
        */
        iterateTree(tree.treeJson, function (tnode) {
            if (tnode.node && tnode.node.id == updated_node.id) {

                if (!tnode.node.schema) {
                    tnode.node.schema = assigned_concept
                    tnode.node.nodeTypeId = assigned_concept.id
                }
                tree._manipulator.refreshNode(tnode, {
                    _free: false
                })
            }
        })

        tree.draw()
    }
}