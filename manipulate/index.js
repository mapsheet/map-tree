module.exports = function (tree) {
    return {
        attachAppendable: require('./attach.appendable')(tree),
        createAttachRoot: require('./create.attach.root')(tree),
        // detachConnectables: require('./detach.connectables')(tree),
        refreshNode: require('./refresh.node')(tree),
        updateConcept: require('./update.concept')(tree),
        addConcept: require('./add.concept')(tree),
        addRelation: require('./add.relation')(tree),
        assignRelation: require('./assign.relation')(tree),
        deleteTnode: require('./remove.tnode')(tree)
    }

}