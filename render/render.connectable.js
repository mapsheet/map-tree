const handleNode = require('../extensions/node/handle.node')

module.exports = function (node_json) {
    return function (dom) {
        if (!node_json._connectable) {
            return
        }
        var connectable = $(dom).find('.connectable')
        var node_label_input = connectable.find('input')
        var add_button = connectable.find('.after-input')

        //toggle input addon based on entry
        node_label_input.keyup(function (e) {
            activateConnectable(node_label_input.val())
            if (e.keyCode == 13) {
                appendIfValid()
            }
        })


        //call event when add button is clicked
        add_button.click(appendIfValid)

        function appendIfValid() {
            var value = node_label_input.val()
            console.log('Entered value in node label input ', value)
            if (value.length > 1) {
                handleNode(node_json, 'append', value)
            }
        }

        //set border color based on the legend
        if (node_json.node) {
            connectable.css('border-color', node_json.node.color)
        }

        //attach autocomplete to the node if it is already conceptualized
        if (!node_json._free) {
            var autocomplete_options = {
                source: node_json._tree.graphJson.nodes.filter(function (node) {
                    return node.nodeTypeId == node_json.node.id
                }).map(function (node) {
                    //get labels of nodes of this node type
                    return node.label
                }),
                minLength: 0,
                select: function (event, ui) {
                    console.log('selection triggered')
                    activateConnectable(ui.item.value)
                }
            }

            jQuery(function () {
                node_label_input.autocomplete(autocomplete_options)
                    .focus(function () {
                        //Use the below line instead of triggering keydown
                        console.log('focused')
                        $(this).autocomplete("search");
                    });
            })
        }

        function activateConnectable(value) {

            console.log('value is ', value)
            if (value.length > 1) {
                connectable.removeClass('connectable-idle').addClass('connectable-active')
            } else {
                connectable.removeClass('connectable-active').addClass('connectable-idle')
            }
        }

    }
}