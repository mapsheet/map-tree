module.exports = {
    up: {
        code: 38,
        on: 'keydown'
    },
    down: {
        code: 40,
        on: 'keydown'
    },
    left: {
        code: 37,
        on: 'keydown'
    },
    right: {
        code: 39,
        on: 'keydown'
    },
    tab: {
        code: 9,
        on: 'keydown'
    },
    space: {
        code: 32,
        on: 'keyup'
    },
    enter: {
        code: 13,
        on: 'keyup'
    }
}