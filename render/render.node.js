//const handlebars = require('handlebars')
const Handlebars = require('handlebars')
const node_template = require('./node.template.html')
const handleNode = require('map-tree/extensions/node/handle.node')
const renderMenu = require('./render.menu')
const renderConnectable = require('./render.connectable')
const renderConceptualize = require('./render.conceptualize')
const renderRelate = require('./render.relate')
const handleTree = require('map-tree/extensions/map/handle.tree')

const _ = require('lodash')

Handlebars.registerHelper("switch", function (value, options) {
    this._switch_value_ = value;
    var html = options.fn(this); // Process the body of the switch block
    delete this._switch_value_;
    return html;
})

Handlebars.registerHelper("case", function (value, options) {
    if (value == this._switch_value_) {
        return options.fn(this);
    }
})

const nodeTemplateHandlebar = Handlebars.compile(node_template);

module.exports = function (node_json) {
    var dom = null,
        node_element = null,
        input_element = null,
        expand_switch = null,
        node_label_text = null
    var render_node = {}
    var renderMenuForNode = renderMenu(node_json)
    var renderConnectableForNode = renderConnectable(node_json)
    var renderConceptualizeNode = renderConceptualize(node_json)
    var renderRelateForNode = renderRelate(node_json)
    //var throttledPan = _.throttle(panToNode, 1000)



    render_node.render = function () {
        var el = null
        if (dom) {
            el = dom
        } else {
            el = document.createElement("div")
        }

        el.classList.add('node')
        el.style.position = "absolute";
        el.innerHTML = nodeTemplateHandlebar(node_json) //mustache.render(node_template, node_json)
        //el.style.minWidth = 9 * (node_json.name.length) + 'px'


        el.style.display = 'none'
        el.id = node_json.tid
        el.style.height = "30px"

        node_element = el.getElementsByClassName('node')[0]

        input_element = $(dom).find('.connectable').find('input')
        if (node_json.node && node_json.node.schema) {
            node_element.style.borderColor = node_json.node.schema.color
        }

        node_json._tree._container.appendChild(el)

        dom = el

        expand_switch = jQuery(dom).find('.toggle-expand') //dom.getElementsByClassName('toggle-expand')[0]

        node_label_text = jQuery(dom).find('.node-label-text')

        expand_switch.click(function () {
            handleNode(node_json, 'toggle')
        })
        if (node_json._incoming) {
            $(dom).find('.outgoing').hide()
        } else {
            $(dom).find('.incoming').hide()
        }

        $(dom).find('.node-label-text').click(function () {
            console.log('clicked')
            handleTree(node_json._tree, 'focus', node_json.tid)
        })


        renderConnectableForNode(dom)
        renderMenuForNode(dom)
        renderConceptualizeNode(dom)
        renderRelateForNode(dom)
    }
    render_node.remove = function () {
        node_json._tree._container.removeChild(dom)
    }

    render_node.drawAt = function (x, y) {
        dom.style.display = "inline-block"
        dom.style.left = x + 'px'
        dom.style.top = y + 'px'
        node_json._x = x
        node_json._y = y
    }
    render_node.show = function () {
        dom.style.display = "inline-block"
    }
    render_node.hide = function () {
        dom.style.display = "none"
    }
    render_node.showExpander = function () {
        expand_switch.css('visibility', 'visible')
    }
    render_node.hideExpander = function () {
        expand_switch.css('visibility', 'hidden')
    }
    render_node.drawCollapsed = function () {
        expand_switch.css('background-color', 'gray')
    }
    render_node.drawExpanded = function () {
        expand_switch.css('background-color', 'white')
    }
    render_node.gainFocus = function () {
        node_element.style.borderWidth = '5px'
        //details_switch.style.visibility = 'visible'
        if (input_element) {
            input_element.focus()
        }
        //for terminal nodes, show delete icon. dont show for root

        if (!node_json._connectable) {
            $(dom).find('.on-focus').css('visibility', 'visible')
        }

    }
    render_node.loseFocus = function () {
        node_element.style.borderWidth = '2px'

        $(dom).find('.on-focus').css('visibility', 'hidden')
    }

    render_node.onClick = function (fn) {
        //dom.addEventListener('click', fn)
        console.log('clicked')
        //handleTree(node_json._tree, 'focus', node_json.tid)
    }
    render_node.remove = function () {
        dom.parentNode.removeChild(dom)
    }
    render_node.conceptualize = function () {
        /*
            1. Show the conceptualizing section
            2. 
        */

        $(dom).find('.conceptualizing').show()
    }

    render_node.relate = function () {
        $(dom).find('.relating').show()
    }

    render_node.clearInput = function () {
        //input_element.val('')
        $(dom).find('.connectable').find('input').val('')
        $(dom).find('.connectable').removeClass('connectable-active').addClass('connectable-idle')
    }



    return render_node


}