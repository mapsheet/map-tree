module.exports = function (tree_map, pruned_json) {
    /*
        1. Detach any existing connectors
        2. Add new connectors
    */

    if (tree_map._connectors) {
        tree_map._connectors.map(function (connector) {
            jsPlumb.deleteConnection(connector)
        })

        tree_map._connectors.length = 0
    } else {
        tree_map._connectors = []
    }

    iterateAndConnectToParent(pruned_json)



    function iterateAndConnectToParent(json) {
        //console.log('connect ', json)
        if (json.parent) {
            var conn = jsPlumb.connect({
                source: json.parent.tid,
                target: json.tid,
                anchors: ["Right", "Left"],
                endpoint: "Rectangle",
                endpointStyle: {

                },
                connector: ["Flowchart", {
                    stub: 50,
                    alwaysRespectStubs: true,
                    cornerRadius: 10
                }]
            })

            tree_map._connectors.push(conn)

            //setTimeout(function () {
            jsPlumb.revalidate(json.tid)
            jsPlumb.revalidate(json.parent.tid)
            // }, 0)
        }

        json.children.map(function (child) {
            iterateAndConnectToParent(child)
        })
    }
}