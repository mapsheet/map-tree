const handleTree = require('../extensions/map/handle.tree')

module.exports = function (node_json) {
    return function (dom) {
        var concept_input = jQuery(dom).find('.assign-concept')
        var conceptualizing = jQuery(dom).find('.conceptualizing')

        var autocomplete_options = {
            source: node_json._tree.graphJson.legend.nodes.map(function (concept) {
                return concept.label
            }),
            select: function (event, ui) {
                console.log('selction triggered from auto complete ', ui)
                onEntryChange(ui.item.value)
                //value from the textbox is not updated until blur, so we are passing form the ui element
            },
            minLength: 0
        }

        jQuery(function () {
            concept_input.autocomplete(autocomplete_options)
                .focus(function () {
                    //Use the below line instead of triggering keydown
                    console.log('focused')
                    $(this).autocomplete("search");
                });
        })


        concept_input.keyup(function () {
            onEntryChange()
        })

        function onEntryChange(value) {
            if (!value) {
                value = concept_input.val()
            }

            console.log('entry changed ', value)
            // console.log('key press in conceptalize ', value)
            if (value.length > 1) {
                // console.log('making active')
                conceptualizing.removeClass('conceptualizing-idle').addClass('conceptualizing-active')
            } else {
                // console.log('making idle')
                conceptualizing.removeClass('conceptualizing-active').addClass('conceptualizing-idle')
            }
        }
        conceptualizing.find('.enter-concept').click(function () {
            var value = concept_input.val()
            handleTree(node_json._tree, 'entered_concept', value)
        })
    }
}