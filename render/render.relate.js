const handleTree = require('../extensions/map/handle.tree')

module.exports = function (node_json) {
    return function (dom) {
        var relation_input = jQuery(dom).find('.assign-relation')
        var relating = jQuery(dom).find('.relating')

        /*
        var autocomplete_options = {
            source: node_json._tree.graphJson.legend.nodes.map(function (concept) {
                return concept.label
            }),
            select: function (event, ui) {
                console.log('selction triggered from auto complete ', ui)
                onEntryChange(ui.item.value)
                //value from the textbox is not updated until blur, so we are passing form the ui element
            },
            minLength: 0
        }

        jQuery(function () {
            relation_input.autocomplete(autocomplete_options)
                .focus(function () {
                    //Use the below line instead of triggering keydown
                    console.log('focused')
                    $(this).autocomplete("search");
                });
        })
        */


        relation_input.keyup(function (e) {
            console.log('pressed key in relate input')
            var value = relation_input.val()

            if (value.length > 1) {
                // console.log('making active')
                relating.removeClass('relating-idle').addClass('relating-active')
            } else {
                // console.log('making idle')
                relating.removeClass('relating-active').addClass('relating-idle')
            }

            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode == '13') {
                console.log('pressed enter in relate input')
                handleTree(node_json._tree, 'entered_relation', value)
            }
        })

    }
}