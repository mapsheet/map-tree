const handleNode = require('../extensions/node/handle.node')
const handleTree = require('../extensions/map/handle.tree')

module.exports = function (node_json) {
    var is_menu_open = false
    return function (dom) {
        if (node_json._connectable) {
            return
        }

        //Hide menu in view mode. Hide target in edit mode
        if (node_json._tree._allow_edit) {
            //in edit mode, hide the target
            $(dom).find('.perspective-switch').hide()
        } else {
            $(dom).find('.menu-switch').hide()
            $(dom).find('.perspective-link').attr('href', window.location.pathname + '?id=' + getQueryStringValue('id') + '&perspective=' + node_json.node.id)
        }

        //Show menu on clicking switch
        $(dom).find('.menu-switch').click(function (e) {
            console.log('clicked menu switch')
            $(dom).find('.menu-list').toggle()
            is_menu_open = !is_menu_open
            //dont propagate this event to the document. else it will be hidden
            e.stopPropagation()
        })

        $('.menu-list').click(function (event) {
            event.stopPropagation()
        })

        //close menu on clicking outside
        $(document).click(function () {
            // console.log('hiding menu list')
            $('.menu-list').hide()
            if (node_json.tid != node_json._tree._focus && is_menu_open) {
                $(dom).find('.on-focus').css('visibility', 'hidden')
                is_menu_open = false
            }
        })

        //Show delete for terminal and non-root node
        if (!node_json.childCount && node_json.parent) {
            $(dom).find('.menu-delete').css('display', 'block')
        }

        //Show conceptualize for free nodes
        if (node_json.node && node_json.node.free) {
            $(dom).find('.menu-conceptualize').css('display', 'block')
        }

        //show conceptualize when node is not free, but edge is free
        if (node_json.node && !node_json.free && node_json.source && node_json.source.free) {
            $(dom).find('.menu-relate').css('display', 'block')
        }

        //Show edit for nodes with schema
        if (node_json.node && node_json.node.schema) {
            $(dom).find('.menu-details').css('display', 'block')
            $(dom).find('.menu-concept').css('display', 'block')
        }

        //show menu switch on mouseover
        $(dom).find('.node-label').mouseenter(function () {
            $(dom).find('.on-focus').css('visibility', 'visible')
        })
        //hide menu switch on mouseout, if node is not the current focus
        $(dom).find('.node-label').mouseleave(function () {
            console.log('mouse has left ', is_menu_open)
            if (node_json.tid != node_json._tree._focus && !is_menu_open) {
                $(dom).find('.on-focus').css('visibility', 'hidden')
            }

        })

        //on clicking conceptualize - conceptualize the node
        $(dom).find('.menu-delete').click(function () {
            handleNode(node_json, 'delete')
        })


        //on clicking delete, delete the node
        $(dom).find('.menu-conceptualize').click(function () {
            handleTree(node_json._tree, 'assign', node_json.tid)
        })

        $(dom).find('.menu-relate').click(function () {
            handleTree(node_json._tree, 'relate', node_json.tid)
        })

        //on clicking edit, edit the node
        $(dom).find('.menu-details').click(function () {
            // console.log('clicked menu edit')
            //handleNode(node_json, 'gain_focus')
            node_json._tree.focus(node_json.tid)
        })
    }
}

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}