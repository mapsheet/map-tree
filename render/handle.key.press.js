const keyCodes = require('./key.codes')
const $ = require('jquery')

module.exports = function (map) {
    Object.keys(keyCodes).map(function (key) {

        onKeyPress(key, function (e) {
            console.log('key up ', key)
            //handleNode(node_json, key + '_key', e)
            map.handleKeypress(key, e)
        })
    })

    function onKeyPress(code, fn) {
        var on = keyCodes[code].on
        $(document)[on](function (e) {
            ////console.log('document key down ', e.which)
            Object.keys(keyCodes).map(function (keyType) {
                if (e.which == keyCodes[keyType].code) {
                    ////console.log('pressed ', keyType, ' key')

                    //e.preventDefault();
                    //calling prevent default to avoid scroll or any other events
                    //side effect is that this will also stop text inputs from working properly for these keys
                    //move this into corresponding event handler

                    if (code == keyType) {
                        fn(e)
                    }
                }
            })
        })

    }
}