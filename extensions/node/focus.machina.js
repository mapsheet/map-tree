const machina = require('machina')
const $ = require('jquery')
const handleNode = require('map-tree/extensions/node/handle.node')

module.exports = function (node_json) {
    return new machina.BehavioralFsm({
        initialState: 'uninitialized',
        states: {
            uninitialized: {
                is_graph_node: 'graph_node_created'
            },
            graph_node_created: {
                _onEnter: function (client) {
                    //console.log('entered graph_node_focus_state')
                    node_json._renderer.loseFocus()
                },
                show: "graph_node_shown",
                gain_focus: 'graph_node_focus'
            },
            graph_node_editing: {
                _onEnter: function () {
                    console.log('node is in editing state')
                },
                viewing: 'graph_node_focus'
            },
            graph_node_focus: {
                hide: 'graph_node_hidden',
                editing: 'graph_node_editing',
                _onEnter: function (client) {
                    //console.log('entered graph_node_focus_state')
                    node_json._renderer.gainFocus()
                },
                lose_focus: 'graph_node_created',
                up_key: function (client, keyboard_event) {
                    //console.log('up key on ', node_json.tid, ' moving to ', node_json._elder)
                    if (node_json._elder) {
                        node_json._tree.focus(node_json._elder)
                    }
                    keyboard_event.preventDefault()
                },
                down_key: function (client, keyboard_event) {
                    //console.log('down key on ', node_json.tid, ' moving to ', node_json._younger)

                    if (node_json._younger) {
                        node_json._tree.focus(node_json._younger)
                    }
                    keyboard_event.preventDefault()
                },
                left_key: function (client, keyboard_event) {
                    //console.log('left key on ', node_json.tid, ' moving to ', node_json.parent.tid)

                    if (node_json.parent) {
                        node_json._tree.focus(node_json.parent.tid)
                    }
                    keyboard_event.preventDefault()

                },
                right_key: function (client, keyboard_event) {
                    console.log('right key on ', node_json.tid)
                    /*
                        1. If node is exapandable, but collapsed, expand it
                        2. If node is expanded, then move to first child
                    */
                    this.expandOrNavigate(client, keyboard_event)
                },
                tab_key: function (client, keyboard_event) {
                    //console.log('tab key on ', node_json.tid, 'expanded  ', node_json._expanded)
                    /*
                        1. If node is exapandable, but collapsed, expand it
                        2. If node is expanded, then move to first child
                    */
                    this.expandOrNavigate(client, keyboard_event)
                },
                space_key: function (client, keyboard_event) {
                    //alert('Enter key on ' + node_json.tid)
                    //console.log('handling space key on the focused node ', client)
                    // if (node_json._expanded) {

                    // }
                    handleNode(client._node, 'expand')
                    node_json._tree.extend()
                    keyboard_event.preventDefault()
                },
                enter_key: function (client, keyboard_event) {
                    node_json._tree.assign(node_json.tid)
                    keyboard_event.preventDefault()
                }

            }
        },
        create: function (client, map_node) {
            var focusMachina = this
            if (!node_json._connectable) {
                focusMachina.handle(client, 'is_graph_node')
            } else {
                focusMachina.handle(client, 'is_schema_node')
            }

            // node_json._renderer.onClick(function () {
            //     //console.log(' node clicked')
            //     if (!node_json._connectable) {
            //         node_json._tree.focus(node_json.tid)
            //     }
            // })

        },
        expandOrNavigate: function (client, keyboard_event) {

            keyboard_event.preventDefault()
            if (node_json._expandable) {
                if (node_json._expanded) {
                    var first_child = null
                    if (node_json.children && node_json.childCount > 0) {
                        first_child = node_json.children[0]
                        if (first_child._shown) {
                            node_json._tree.focus(first_child.tid)
                        }

                    }
                } else {
                    ////console.log('branchingMachina ', branchingMachina)
                    //node_json.extensions.branching.machina.expand(client._node)
                    handleNode(client._node, 'expand')
                }
            }
            console.log('calling prevent default')
        }
    })
}