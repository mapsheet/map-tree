const machina = require('machina')
//const drawConcept = require('../manipulate/update.concept')
const Handlebars = require('handlebars')
const _ = require('lodash')

const routes = {
    conceptualizing: '/{{legendId}}/{{graphId}}/{{tid}}/concept/assign/{{nodeId}}',
    relating: '/{{legendId}}/{{graphId}}/{{tid}}/relation/assign/{{nodeId}}/{{edgeId}}',
    detailing: '/{{legendId}}/{{graphId}}/{{tid}}/tnode/{{conceptId}}/{{relationId}}/{{nodeId}}/{{edgeId}}',
    idle: '/'
}

var route_handlebars = {}

Object.keys(routes).map(function (state) {
    route_handlebars[state] = Handlebars.compile(routes[state])
})

module.exports = function (node_json) {
    return new machina.BehavioralFsm({

        initialState: 'uninitialized',
        states: {
            uninitialized: {
                _onEnter: function (client) {
                    console.log('Entering uninitialized state in edit machina ', client._node)
                },
                enter_key: function (client) {
                    if (!node_json._connectable) {
                        this.transition(client, 'in')
                    }
                }
            },
            idle: {
                _onEnter: function (client) {
                    routeTo('idle')(client)
                },
                enter_key: function (client) {
                    if (!node_json._connectable) {
                        this.transition(client, 'in')
                    }
                }
            },
            in: {
                _onEnter: function (client) {
                    console.log('editing machina enters in state')
                    //call free_node or has_concept based on schema
                    var editor = node_json._tree._editor
                    editor._json = client._node
                    if (node_json.node.free) {
                        this.handle(client, 'free_node')
                    } else {
                        this.handle(client, 'has_concept')
                    }
                },
                lose_focus: 'out',
                free_node: 'conceptualizing',
                has_concept: 'conceptualized'
            },
            out: {
                _onEnter: function (client) {
                    console.log('editing machina is leaving in state')
                    var editor = node_json._tree._editor
                    editor._json = null
                },
                gain_focus: 'in'
            },
            conceptualizing: {
                _onEnter: routeTo('conceptualizing'),
                assigned_concept: function (client, node, concept) {
                    node_json._tree._manipulator.updateConcept(client._node, node, concept)
                    this.transition(client, 'relating')
                },
                created_concept: function (client, concept) {
                    node_json._tree._manipulator.addConcept(concept)
                },
                close: 'idle'
            },
            conceptualized: {
                _onEnter: function (client) {
                    //check if there relation is defined
                    if (node_json.source && node_json.source.free) {
                        this.handle(client, 'free_edge')
                    } else {
                        this.handle(client, 'has_relation')
                    }
                },
                has_relation: 'detailing',
                free_edge: 'relating',
                close: 'idle'
            },
            relating: {
                _onEnter: routeTo('relating'),
                close: 'idle',
                created_relation: function (client, relation) {
                    drawNewRelation(relation)
                },
                assigned_relation: function (client, edge, relation) {
                    drawAssignedRelation(edge, relation)
                    this.transition(client, 'detailing')
                }
            },
            detailing: {
                _onEnter: routeTo('detailing'),
                close: 'idle'
            }
        },
        create: function (client, map_node) {

        }
    })

    function routeTo(state) {

        return function (client) {
            var json = {}
            json.tid = node_json.tid
            json.legendId = _.get(client, ['_node', '_tree', 'graphJson', 'legend', 'id']) //node_json._tree.graphJson.legend.id
            json.graphId = _.get(client, ['_node', '_tree', 'graphJson', 'id']) //node_json._tree.graphJson.id
            json.nodeId = _.get(client, ['_node', 'node', 'id']) //node_json.node.id 
            json.conceptId = _.get(client, ['_node', 'node', 'schema', 'id']) //node_json.node.schema.id
            json.relationId = _.get(client, ['_node', 'source', 'edgeTypeId']) || 'root'
            var editor = _.get(client, ['_node', '_tree', '_editor']) //node_json._tree._editor
            editor._json = client._node

            json.edgeId = _.get(client, ['_node', 'source', 'id']) || 'root'

            if (editor.path) {
                editor.path(route_handlebars[state](json))
            }

        }

    }
}