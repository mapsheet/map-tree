const machina = require('machina')
const connectableToAppendable = require('map-tree/compute/connectable.to.appendable')
// const attachAppendable = require('../manipulate/attach.appendable')
//const createAttachRoot = require('../manipulate/create.attach.root')

module.exports = function (node_json) {

    return new machina.BehavioralFsm({
        initialState: 'uninitialized',
        states: {
            uninitialized: {
                is_connectable: 'idle_connectable',
                not_connectable: 'not_connectable'
            },
            not_connectable: {},
            idle_connectable: {
                _onEnter: function () {
                    // console.log('connectable is in idle_connectable')
                },
                append: function (client, entered_text) {
                    //check if enough data has been entered
                    console.log('handling enter key in connectable')
                    if (entered_text.length > 1) {
                        node_json._appending = entered_text

                        if (node_json._isRoot) {
                            this.transition(client, 'rooting')
                        } else {

                            this.transition(client, 'extending')
                        }

                    }
                }
            },
            rooting: {
                _onEnter: function (client) {
                    node_json._tree._manipulator.createAttachRoot(node_json, node_json._appending)
                }
            },
            extending: {
                _onEnter: function (client) {
                    //to do - show node as being created
                    var populateMachina = this
                    //send data to server to create new node
                    //call created with appendable
                    connectableToAppendable(client._node, node_json._appending)
                        .then(function (appendable) {
                            node_json._tree._manipulator.attachAppendable(appendable)
                            populateMachina.handle(client, 'appended')
                        })
                },
                appended: 'appended'
            },
            appended: {
                _onEnter: function (client) {
                    //to do - check this this connectable is required
                    //if not, mark it as not needed
                    //draw this map again
                    node_json._renderer.clearInput()

                    this.handle(client, 'redraw')
                },
                redraw: function (client) {

                    node_json._tree.draw()

                    this.transition(client, 'idle_connectable')
                },
                exhausted: function (client) {
                    node_json._shown = false
                    node_json._tree.draw()

                    this.transition(client, 'idle_connectable')
                }
            }
        },
        create: function (client, map_node) {
            var populateMachina = this
            if (node_json._connectable) {
                populateMachina.handle(client, 'is_connectable')
            }
        }
    })
}