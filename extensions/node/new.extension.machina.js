const machina = require('machina')

module.exports = function (node_json) {
    return new machina.BehavioralFsm({
        initialState: 'uninitialized',
        states: {
            uninitialized: {
                initialize: 'idle'
            },
            idle: {}
        },
        create: function (client, map_node) {
            this.handle(client, 'initialize')
        }
    })
}