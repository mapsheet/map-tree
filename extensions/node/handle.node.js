module.exports = function (node, event, input, input1, input2, input3) {
    if (node.extensions) {
        Object.keys(node.extensions).map(function (ext) {
            var extension = node.extensions[ext]
            extension.machina.handle(extension.client, event, input, input1, input2, input3)
        })
    }
}