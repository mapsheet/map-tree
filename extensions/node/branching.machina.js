const machina = require('machina')
const handleNode = require('map-tree/extensions/node/handle.node')

module.exports = function (node_json) {
    return new machina.BehavioralFsm({
        initialState: 'uninitialized',
        states: {
            uninitialized: {
                has_children: 'expandable',
                no_children: 'terminal'
            },
            terminal: {
                _onEnter: function (client) {
                    node_json._expandable = false
                    node_json._renderer.hideExpander()
                }
            },
            expandable: {
                _onEnter: function (client) {
                    node_json._expandable = true
                    //node_json._expanded = true
                    node_json._renderer.showExpander()

                },
                collapse: 'collapsed',
                expand: 'expanded'
            },
            collapsed: {
                _onEnter: function (client) {
                    //set all children as hidden
                    var initially_expanded = node_json._expanded
                    node_json._expanded = false
                    node_json._renderer.drawCollapsed()
                    node_json.children.map(function (child) {
                        handleNode(child, 'hide')
                    })
                    if (initially_expanded) {
                        node_json._tree.draw()
                    }

                },
                toggle: 'expanded',
                expand: 'expanded'
            },
            expanded: {
                _onEnter: function (client) {
                    // console.log('this is now expanded')
                    var node = client._node
                    //make sure that node is shown before expanding
                    handleNode(node, 'show')
                    var initially_expanded = node_json._expanded
                    node_json._expanded = true
                    node_json._renderer.drawExpanded()
                    node_json.children.map(function (child) {
                        //when node is expanded, move all children to shown state
                        handleNode(child, 'show')
                    })
                    if (!initially_expanded) {
                        node_json._tree.draw()
                    }

                },
                toggle: 'collapsed'
            }

        },
        create: function (client, map_node) {
            var branch = this

            if (map_node.children && map_node.childCount > 0) {
                branch.handle(client, 'has_children')
            } else {
                branch.handle(client, 'no_children')
            }

        }
    })
}