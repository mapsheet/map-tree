const machina = require('machina')
const mapClient = require('map-client')

module.exports = function (node_json) {
    return new machina.BehavioralFsm({
        initialState: 'uninitialized',
        states: {
            uninitialized: {
                initialize: 'idle'
            },
            idle: {
                delete: 'confirming'
            },
            confirming: {
                _onEnter: function (client) {
                    if (window.confirm('Are you sure you want to delete this node?')) {
                        this.handle(client, 'delete_confirmed')
                    } else {
                        this.handle(client, 'delete_rejected')
                    }
                },
                delete_confirmed: 'deleting',
                delete_rejected: 'idle'
            },
            deleting: {
                _onEnter: function (client) {
                    const deleteMachina = this
                    mapClient.deleteEdge(node_json.source)
                        .then(function () {
                            deleteMachina.handle(client, 'deleted')
                        })
                },
                deleted: 'deleted'
            },
            deleted: {
                _onEnter: function (client) {
                    node_json._tree._manipulator.deleteTnode(node_json)
                    this.transition(client, 'idle')
                }
            }
        },
        create: function (client, map_node) {
            this.handle(client, 'initialize')
        }
    })
}