const extensions_fn = {
    assign: require('./assign.machina')
    // extend: require('./extend.machina')
}

module.exports = function (map) {
    var extensions = {}
    Object.keys(extensions_fn).map(function (key) {
        extensions[key] = extensions_fn[key](map)
    })

    map.extensions = {}

    Object.keys(extensions).map(function (key) {
        var client = {
            _map: map
        }
        extensions[key].create(client)
        map.extensions[key] = {
            client: client,
            machina: extensions[key]
        }
    })
}