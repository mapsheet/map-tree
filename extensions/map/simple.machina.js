const machina = require('machina')

module.exports = machina.Fsm.extend({
    initialize: function (map) {
        this._map = map
    },
    initialState: 'up',
    states: {
        up: {
            _onEnter: function () {
                console.log('into up state')
            },
            go_down: 'down'
        },
        down: {
            _onEnter: function () {
                console.log('into down state')
            },
            go_up: 'up'
        }
    }
})