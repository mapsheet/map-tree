module.exports = function (tree, event, input, input1, input2, input3) {
    tree.handle(event, input, input1, input2, input3)
    if (tree.extensions) {
        Object.keys(tree.extensions).map(function (ext) {
            var extension = tree.extensions[ext]
            extension.machina.handle(extension.client, event, input, input1, input2, input3)
        })
    }
}