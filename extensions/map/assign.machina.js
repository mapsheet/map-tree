const machina = require('machina')
//const drawConcept = require('../manipulate/update.concept')
const Handlebars = require('handlebars')
const _ = require('lodash')
const bluebird = require('bluebird')
const mapClient = require('map-client')

const routes = {
    conceptualizing: '/{{legendId}}/{{graphId}}/{{tid}}/concept/assign/{{nodeId}}',
    relating: '/{{legendId}}/{{graphId}}/{{tid}}/relation/assign/{{nodeId}}/{{edgeId}}',
    detailing: '/{{legendId}}/{{graphId}}/{{tid}}/tnode/{{conceptId}}/{{relationId}}/{{nodeId}}/{{edgeId}}',
    idle: '/'
}

var route_handlebars = {}

Object.keys(routes).map(function (state) {
    route_handlebars[state] = Handlebars.compile(routes[state])
})



module.exports = function (map) {
    return new machina.BehavioralFsm({
        initialize: function () {
            console.log('assign machina initialized')
        },
        initialState: 'uninitialized',
        states: {
            uninitialized: {
                initialize: 'idle'
            },
            idle: {
                _onEnter: function (client) {
                    console.log('assigning machina is in idle')
                    map._assigning = null
                    var editor = map._editor
                    editor._json = null
                },
                assign: function (client, tid) {
                    map._assigning = map.flatNodes[tid]
                    this.transition(client, 'begin')
                },
                relate: function (client, tid) {
                    map._assigning = map.flatNodes[tid]
                    this.transition(client, 'begin')
                }
            },
            begin: {
                _onEnter: function (client) {
                    console.log('editing machina enters in state')
                    //call free_node or has_concept based on schema
                    var editor = map._editor
                    editor._json = map._assigning

                    if (map._assigning.node.free) {
                        this.handle(client, 'free_node')
                    } else {
                        this.handle(client, 'has_concept')
                    }
                },
                free_node: 'conceptualizing',
                has_concept: 'conceptualized'
            },
            conceptualizing: {
                _onEnter: function () {
                    //routeTo('conceptualizing'),   
                    console.log('enter conceptualizing')
                    map._assigning._renderer.conceptualize()
                },
                entered_concept: function (client, conceptLabel) {
                    var assigned_concept = null
                    var assignMachina = this
                    var node = map._assigning.node
                    findOrCreateConcept(conceptLabel, assignMachina, client)
                        .then(function (concept) {
                            assigned_concept = concept
                            return mapClient.assignNodeType(node.id, concept.id)
                        }).then(function (updated_node) {
                            //call handle on edit machina to take updated node and schema
                            //handleNode(map._editor._json, 'assigned_concept', updated_node, assigned_concept)
                            assignMachina.handle(client, 'assigned_concept', updated_node, assigned_concept)
                        })
                },

                created_concept: function (client, concept) {
                    map._manipulator.addConcept(concept)
                },
                assigned_concept: function (client, node, concept) {
                    map._manipulator.updateConcept(map._assigning, node, concept)
                    if (map._assigning.parent) {
                        this.handle(client, 'assign_relation')
                    } else {
                        this.handle(client, 'is_root')
                    }


                },
                close: 'idle',
                is_root: 'idle',
                assign_relation: 'relating'
            },
            conceptualized: {
                _onEnter: function (client) {
                    //check if there relation is defined
                    if (map._assigning.parent) {
                        if (map._assigning.source && map._assigning.source.free) {
                            this.handle(client, 'free_edge')
                        } else {
                            this.handle(client, 'has_relation')
                        }
                    } else {
                        this.handle(client, 'is_root')
                    }

                },
                has_relation: 'idle',
                free_edge: 'relating',
                close: 'idle',
                is_root: 'idle'
            },
            relating: {
                _onEnter: function () {
                    console.log('Entered relating')
                    map._assigning._renderer.relate()
                },
                close: 'idle',
                entered_relation: function (client, relationLabel) {
                    var assigned_relation = null
                    var assignMachina = this
                    var to_node = map._assigning.node
                    var from_node = map._assigning.parent.node
                    var edge = map._assigning.source
                    var legendId = map.graphJson.legend.id
                    mapClient.createRelation(from_node, to_node, legendId, relationLabel)
                        .then(function (relation) {
                            assignMachina.handle(client, 'created_relation', relation)
                            assigned_relation = relation
                            return mapClient.assignRelation(edge.id, relation.id)
                        }).then(function (updated_relation) {
                            //call handle on edit machina to take updated node and schema
                            //handleNode(map._editor._json, 'assigned_concept', updated_node, assigned_concept)
                            assignMachina.handle(client, 'assigned_relation', updated_relation, assigned_relation)
                        })
                },
                created_relation: function (client, relation) {
                    map._manipulator.addRelation(relation)
                },
                assigned_relation: function (client, edge, relation) {
                    //drawAssignedRelation(edge, relation, map._assigning)
                    map._manipulator.assignRelation(edge, relation, map._assigning)
                    this.transition(client, 'idle')
                }
            }
        },
        create: function (client) {
            var assignMachina = this
            assignMachina.handle(client, 'initialize')
        }
    })


    function findOrCreateConcept(label, assignMachina, client) {
        /*  
            1. Check if this concept already exists
            2. If exists, resolve with the matching concept object
            3. If does not exist, resolve with 
        */
        var concepts = map.graphJson.legend.nodes
        var matching_concepts = concepts.filter(function (concept) {
            return concept.label.toLowerCase() == label.toLowerCase()
        })

        if (matching_concepts.length == 1) {
            return bluebird.resolve(matching_concepts[0])
        } else {
            var legendId = map.graphJson.legend.id
            return mapClient.createNewNodeType(label, legendId)
                .then(function (new_concept) {
                    //handleNode(map._editor._json, 'created_concept', new_concept)
                    assignMachina.handle(client, 'created_concept', new_concept)
                    return bluebird.resolve(new_concept)
                })
        }
    }

    function routeTo(state) {

        return function (client) {
            var json = {}
            json.tid = map._assigning.tid
            json.legendId = _.get(map, ['_assigning', '_tree', 'graphJson', 'legend', 'id']) //node_json._tree.graphJson.legend.id
            json.graphId = _.get(map, ['_assigning', '_tree', 'graphJson', 'id']) //node_json._tree.graphJson.id
            json.nodeId = _.get(map, ['_assigning', 'node', 'id']) //node_json.node.id 
            json.conceptId = _.get(map, ['_assigning', 'node', 'schema', 'id']) //node_json.node.schema.id
            json.relationId = _.get(map, ['_assigning', 'source', 'edgeTypeId']) || 'root'
            var editor = _.get(map, ['_assigning', '_tree', '_editor']) //node_json._tree._editor
            editor._json = map._assigning

            json.edgeId = _.get(map, ['_assigning', 'source', 'id']) || 'root'

            if (editor.path) {
                editor.path(route_handlebars[state](json))
            }

        }

    }
}