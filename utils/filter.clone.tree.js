module.exports = function (tree, fn) {
    //iterate through the whole tree and children
    //retain nodes if the function returns true

    var filtered = {}
    copyAndIterateChildren(tree, filtered)
    return filtered

    function copyAndIterateChildren(src, dst) {
        Object.assign(dst, src)
        dst.children = []

        src.children.filter(function (child) {
            return fn(child)
        }).map(function (child) {
            var json = {}
            dst.children.push(json)
            copyAndIterateChildren(child, json)
        })
    }
}