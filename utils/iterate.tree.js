module.exports = function (json, fn) {

    callAndIterate(json, 0)


    function callAndIterate(root, level) {
        fn(root, level)

        root.children.map(function (child) {
            callAndIterate(child, level + 1)
        })
    }
}