const iterateTree = require('map-tree/utils/iterate.tree')

module.exports = function (pruned_json, flatNodes) {
    var nodes_at_level = {}
    iterateTree(pruned_json, function (json, level) {
        if (!nodes_at_level[level]) {
            nodes_at_level[level] = []
        }
        nodes_at_level[level].push(json)
    })

    Object.keys(nodes_at_level).map(function (level) {
        var nodes_at_this_level = nodes_at_level[level]
        nodes_at_this_level.map(function (child, index) {
            var next_index = (index + 1) % (nodes_at_this_level.length)
            var prev_index = (nodes_at_this_level.length + index - 1) % (nodes_at_this_level.length)

            var original_node = flatNodes[child.tid]
            //make changes in the original node as pruned_json is only used for assigning positions
            if (original_node) {
                original_node._younger = nodes_at_this_level[next_index].tid
                original_node._elder = nodes_at_this_level[prev_index].tid
            }
        })
    })
}