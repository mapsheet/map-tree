module.exports = function (map, json) {
    /*
        1. Get all edge_types that are associated with the current nodes node type
        2. Get cardinality of the other side
        3. If cardinality of the other side is 1, check that there is only one edge of this type

    */

    const graph_json = map.graphJson
    if (!json) {
        json = map.flatNodes[map._focus]
    }
    if (json._connectable) {
        //this node is already a connectable
        return
    }
    var placeholders = [] //return an array with node type, edge type and direction
    var legend = graph_json.legend
    var node = json.node
    map.connectables = []
    //console.log('node ', node)

    var possible_edges = legend.edges.filter(function (edge_type) {
        //check that this node can fit on either side of the edge
        return edge_type.fromId == node.nodeTypeId || edge_type.toId == node.nodeTypeId
    })

    //console.log('possible edges ', possible_edges)
    possible_edges.map(function (edge_type) {
        //check if cardinality of the other side is 1
        ////console.log('testing for ', getNodeType(edge_type.fromId).label, edge_type.label, getNodeType(edge_type.toId).label)
        if (edge_type.fromId == node.nodeTypeId) {
            //this node is at the source
            if (checkValidSource(edge_type)) {
                placeholders.push({
                    edge_type: edge_type,
                    as: 'target', //node is a source, then placeholder is target
                    node_type: getNodeType(edge_type.toId)
                })
            }
        }

        if (edge_type.toId == node.nodeTypeId && edge_type.fromId != edge_type.toId) {

            if (checkValidTarget(edge_type)) {
                placeholders.push({
                    edge_type: edge_type,
                    as: 'source', //node is a target, then placeholder is a source
                    node_type: getNodeType(edge_type.fromId)
                })
            }

        }
    })


    placeholders.map(function (connectable) {
        //console.log('Adding connnectable on ', json.tid, ' having ' + json.children.length + ' children')
        var connectable_id = json.tid + '_as_' + connectable.as + '_to_' + connectable.edge_type.id
        var connectable_node = {
            tid: connectable_id,
            name: connectable.node_type.label,
            via: connectable.edge_type.label,
            source: connectable.edge_type,
            parent: json,
            _connectable: true,
            children: [],
            node: connectable.node_type,
            _as: connectable.as
        }

        map.connectables.push(connectable_node)
    })

    //add a free node
    map.connectables.push({
        tid: json.tid + '_free',
        name: 'What else?',
        via: 'xx',
        parent: json,
        _connectable: true,
        _free: true,
        children: [],
        _as: 'target'
    })

    map.connectables.map(function (connectable_node) {
        // nodeMachina.create(connectable_node, map)//this function is moved to tree machina
        //connectables.push(connectable_node)
        if (json.children) {
            json.children.push(connectable_node)
        } else {
            Object.assign(json, connectable_node)
        }

        map.flatNodes[connectable_node.tid] = connectable_node
    })

    //return connectables

    function checkValidSource(edge_type) {
        var target_is_one = edge_type.cardinality.indexOf('to_one') >= 0

        if (target_is_one) {
            //check that such an edge is not already present
            //if there is such an edge, return false
            return graph_json.edges.filter(function (edge) {
                return edge.fromId == node.id && edge.edgeTypeId == edge_type.id
            }).length == 0
        }
        return true
    }

    function checkValidTarget(edge_type) {
        var source_is_one = edge_type.cardinality.indexOf('one_to') >= 0

        if (source_is_one) {
            //check that such an edge is not already present
            //if there is such an edge, return false
            return graph_json.edges.filter(function (edge) {
                return edge.toId == node.id && edge.edgeTypeId == edge_type.id
            }).length == 0
        }
        return true
    }

    function getNodeType(node_type_id) {
        return graph_json.legend.nodes.filter(function (node_type) {
            return node_type.id == node_type_id
        })[0]
    }




}