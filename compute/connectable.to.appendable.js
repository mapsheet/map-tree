const bluebird = require('bluebird')
const mapClient = require('map-client')
const _ = require('lodash')

const edge_keys = {
    target: {
        fromId: 'fromId',
        toId: 'toId'
    },
    source: {
        fromId: 'toId',
        toId: 'fromId'
    }
}
/*
    connectable is the tnode corresponding to the connectable

    appendable is an object with
    - as - target/source
    - at - id of the node where it is be be attached
    - node - newly created node to be appended to the map
    - edge - newly created edge to be appended to the map

    Check if new node has to be created, or only edge has to be created with existing node
*/

module.exports = function (connectable, node_label) {
    console.log('append ', node_label, ' to ', connectable)
    var appendable = {
        as: connectable._as,
        at: connectable.parent.node.id
    }
    /*
        1. Find a matching node, or create if needed
        2. Create an edge with the edge type and required direction
    */
    var nodeTypeId = null

    return findOrCreateNode(node_label, _.get(connectable, ['node', 'id']), connectable._free)
        .then(function (created_node) {
            appendable.node = created_node
            appendable.node.schema = connectable.node

            var edge_to_create = {
                properties: {},
                graphId: connectable._tree._id
            }
            if (!connectable._free) {
                edge_to_create.edgeTypeId = connectable.source.id
            } else {
                edge_to_create.free = true
            }

            //fromId, toId can be interchanged based on direction of the edge
            edge_to_create[edge_keys[connectable._as].fromId] = connectable.parent.node.id
            edge_to_create[edge_keys[connectable._as].toId] = created_node.id

            return mapClient.createEdge(edge_to_create)
        }).then(function (created_edge) {
            appendable.edge = created_edge
            appendable.edge.schema = connectable.source
            return bluebird.resolve(appendable)
        })

    /*
        1. Check if a matching node with the node label, of the node type already exists.
        2. If it exists, reuse that node
        3. If it does not exist, create a new one
    */
    function findOrCreateNode(node_label, nodeTypeId, is_free) {
        var node_to_create = {
            properties: {},
            label: node_label,
            graphId: connectable._tree._id
        }

        if (is_free) {
            node_to_create.free = true
            return mapClient.createNode(node_to_create)
        } else {
            var matching_node = connectable._tree.graphJson.nodes.find(function (node) {
                return node.label.toLowerCase().trim() == node_label.toLowerCase().trim() && node.nodeTypeId == nodeTypeId
            })
            if (matching_node) {
                return bluebird.resolve(matching_node)
            } else {
                node_to_create.nodeTypeId = connectable.node.id

                return mapClient.createNode(node_to_create)
            }
        }

    }
}